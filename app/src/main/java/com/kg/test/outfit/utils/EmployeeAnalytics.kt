package com.kg.test.outfit.utils

import com.kg.test.outfit.dao.Gender
import com.kg.test.outfit.vao.Employee
import java.util.*

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class EmployeeAnalytics {
    companion object {
        fun getAverageAge(employees: List<Employee>): Double {
            var sumAge = 0
            for (employee in employees) {
                sumAge += employee.getAge()
            }
            return sumAge / employees.size.toDouble()
        }

        fun getMedianAge(employees: List<Employee>): Double {
            var numArray: Array<Double> = emptyArray()
            for (employee in employees) {
                numArray += employee.getAge().toDouble()
            }
            return calculateMedian(numArray)
        }

        private fun calculateMedian(numArray: Array<Double>): Double {
            Arrays.sort(numArray)
            return if (numArray.isEmpty()) {
                0.0
            } else {
                if (numArray.size % 2 == 0) {
                    (numArray[numArray.size / 2] + numArray[numArray.size / 2 - 1]) / 2
                } else {
                    numArray[numArray.size / 2]
                }
            }
        }

        fun getMaxSalary(employees: List<Employee>): Double {
            var maxSalary = 0.0
            for (employee in employees) {
                if (employee.salary > maxSalary) {
                    maxSalary = employee.salary
                }
            }
            return maxSalary
        }

        fun getMaleFemaleRatio(employees: List<Employee>): String {
            var maleEmployees = 0
            var femaleEmployees = 0
            for (employee in employees) {
                if (employee.gender() == Gender.MALE) {
                    maleEmployees++
                } else {
                    femaleEmployees++
                }
            }
            return "$maleEmployees : $femaleEmployees"
        }
    }
}