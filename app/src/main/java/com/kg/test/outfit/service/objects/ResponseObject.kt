package com.kg.test.outfit.service.objects

/**
 * Created by KlemenG on 21. 04. 2020.
 */
class ResponseObject(
    var isError: Boolean,
    var throwable: Throwable?,
    var result : SearchResult?
)