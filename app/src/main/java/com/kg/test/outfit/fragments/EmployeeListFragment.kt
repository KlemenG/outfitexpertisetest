package com.kg.test.outfit.fragments

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.kg.test.outfit.OnCardClickListener
import com.kg.test.outfit.OutfitAppApplication
import com.kg.test.outfit.R
import com.kg.test.outfit.adapters.EmployeesAdapter
import com.kg.test.outfit.data.EmployeeViewModel
import com.kg.test.outfit.databinding.FragmentEmployeeListBinding
import com.kg.test.outfit.utils.MyViewModelFactory
import com.kg.test.outfit.utils.ToolbarUtils

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class EmployeeListFragment : Fragment(), OnCardClickListener, View.OnClickListener {
    companion object {
        const val EXTRA_EMPLOYEE_ID: String = "employee.id"
    }

    private var _binding: FragmentEmployeeListBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmployeeListBinding.inflate(inflater, container, false)

        ToolbarUtils.initToolbar(
            activity as AppCompatActivity,
            binding.toolbar,
            getString(R.string.title_employee_list),
            displayHome = true,
            displayTitle = true
        )
        binding.toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        binding.fab.setOnClickListener(this)

        val app = activity?.application as OutfitAppApplication
        val employeeViewModel: EmployeeViewModel by activityViewModels {
            MyViewModelFactory(app)
        }

        val adapter = EmployeesAdapter(context, this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        employeeViewModel.allEmployees.observe(
            viewLifecycleOwner,
            Observer { employees ->
                employees?.let { adapter.setEmployees(it) }
            })

        changePlusIconColor()

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun changePlusIconColor() {
        val willBeWhite: Drawable = binding.fab.drawable
        willBeWhite.mutate().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        binding.fab.setImageDrawable(willBeWhite)
    }

    override fun onCardClick(view: View?) {
        val id: Long = view?.tag as Long
        val bundle = Bundle()
        bundle.putLong(EXTRA_EMPLOYEE_ID, id)
        Navigation.findNavController(view).navigate(R.id.action_open_employee_details, bundle)
    }

    override fun onClick(v: View?) {
        if (v == binding.fab) {
            Navigation.findNavController(v).navigate(R.id.action_open_insert_employee)
        }
    }
}