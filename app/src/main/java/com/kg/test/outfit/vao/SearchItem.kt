package com.kg.test.outfit.vao

import com.google.gson.annotations.SerializedName

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class SearchItem(
    @SerializedName("title")
    var title: String
)