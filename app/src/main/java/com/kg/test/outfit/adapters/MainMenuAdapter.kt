package com.kg.test.outfit.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kg.test.outfit.OnCardClickListener
import com.kg.test.outfit.R
import com.kg.test.outfit.dao.MenuItem

class MainMenuAdapter(
    private val menuItems: Array<MenuItem>,
    private val context: Context?,
    private val onCardClickCallback: OnCardClickListener
) : RecyclerView.Adapter<MainMenuAdapter.ViewHolder>(), View.OnClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_menu_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val menuItem = menuItems[position]
        val vh: ViewHolder = holder

        vh.title.text = context?.getString(menuItem.titleRes)
        vh.cardView.tag = menuItem
        vh.cardView.setOnClickListener(this)
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tvTitle)
        val cardView: CardView = itemView.findViewById(R.id.cardView)
    }

    override fun onClick(v: View?) {
        onCardClickCallback.onCardClick(v)
    }
}