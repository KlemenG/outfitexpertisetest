package com.kg.test.outfit.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.kg.test.outfit.OutfitAppApplication
import com.kg.test.outfit.R
import com.kg.test.outfit.data.EmployeeViewModel
import com.kg.test.outfit.databinding.FragmentEmployeeAnalyticsBinding
import com.kg.test.outfit.utils.EmployeeAnalytics
import com.kg.test.outfit.utils.MyViewModelFactory
import com.kg.test.outfit.utils.ToolbarUtils

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class EmployeeAnalyticsFragment : Fragment() {
    private var _binding: FragmentEmployeeAnalyticsBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmployeeAnalyticsBinding.inflate(inflater, container, false)

        ToolbarUtils.initToolbar(
            activity as AppCompatActivity,
            binding.toolbar,
            getString(R.string.title_employee_analytics),
            displayHome = true,
            displayTitle = true
        )
        binding.toolbar.setNavigationOnClickListener { activity?.onBackPressed() }

        binding.dvAverage.setTitle(getString(R.string.average_age))
        binding.dvMedianAge.setTitle(getString(R.string.median_age))
        binding.dvMaleFemaleRatio.setTitle(getString(R.string.male_female_ratio))
        binding.dvMaxSalary.setTitle(getString(R.string.max_salary))

        val app = activity?.application as OutfitAppApplication
        val employeeViewModel: EmployeeViewModel by activityViewModels {
            MyViewModelFactory(app)
        }
        employeeViewModel.allEmployees.observe(viewLifecycleOwner, Observer { employees ->
            if (employees.isNotEmpty()) {
                binding.dvAverage.setValue(EmployeeAnalytics.getAverageAge(employees).toString())
                binding.dvMedianAge.setValue(EmployeeAnalytics.getMedianAge(employees).toString())
                binding.dvMaxSalary.setValue(EmployeeAnalytics.getMaxSalary(employees).toString())
                binding.dvMaleFemaleRatio.setValue(EmployeeAnalytics.getMaleFemaleRatio(employees))
            } else {
                binding.dvAverage.visibility = View.GONE
                binding.dvMedianAge.visibility = View.GONE
                binding.dvMaxSalary.visibility = View.GONE
                binding.dvMaleFemaleRatio.visibility = View.GONE
            }
        })

        return binding.root
    }
}