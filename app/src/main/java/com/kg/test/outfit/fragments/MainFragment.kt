package com.kg.test.outfit.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.kg.test.outfit.OnCardClickListener
import com.kg.test.outfit.R
import com.kg.test.outfit.adapters.MainMenuAdapter
import com.kg.test.outfit.dao.MenuItem
import com.kg.test.outfit.databinding.FragmentMainBinding

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class MainFragment : Fragment(), OnCardClickListener {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)

        val menuItems = enumValues<MenuItem>()

        val adapter =
            MainMenuAdapter(menuItems, context, this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCardClick(view: View?) {
        if (view?.tag is MenuItem) {
            when (view.tag) {
                MenuItem.ITEM_EMPLOYEE_LIST -> {
                    Navigation.findNavController(view).navigate(R.id.action_open_employee_list)
                }
                MenuItem.ITEM_EMPLOYEE_STATISTICS -> {
                    Navigation.findNavController(view).navigate(R.id.action_open_analytics)
                }
            }
        }
    }
}