package com.kg.test.outfit.data

import androidx.lifecycle.LiveData
import com.kg.test.outfit.dao.EmployeeDao
import com.kg.test.outfit.vao.Employee

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class EmployeeRepository(
    private val employeeDao: EmployeeDao
) {
    val allEmployees: LiveData<List<Employee>> = employeeDao.getEmployees()

    fun getEmployeeById(employeeId: Long): LiveData<Employee> {
        return employeeDao.getEmployeeById(employeeId)
    }

    suspend fun insertEmployee(employee: Employee) {
        employeeDao.insertEmployee(employee)
    }

    suspend fun deleteEmployee(employee: Employee) {
        employeeDao.deleteEmployee(employee)
    }
}