package com.kg.test.outfit.fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.kg.test.outfit.OutfitAppApplication
import com.kg.test.outfit.R
import com.kg.test.outfit.dao.Gender
import com.kg.test.outfit.data.EmployeeViewModel
import com.kg.test.outfit.databinding.FragmentInsertEmployeeBinding
import com.kg.test.outfit.utils.JodaUtils
import com.kg.test.outfit.utils.MyViewModelFactory
import com.kg.test.outfit.utils.ToolbarUtils
import com.kg.test.outfit.utils.Utility
import com.kg.test.outfit.vao.Employee
import org.joda.time.DateTime

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class InsertEmployeeFragment : Fragment(), View.OnClickListener, View.OnTouchListener,
    OnDateSetListener {
    private var timestampClick: Long = 0
    private var _binding: FragmentInsertEmployeeBinding? = null
    private val binding get() = _binding!!
    private lateinit var employeeViewModel: EmployeeViewModel
    private var newDateMeasured: DateTime
    private var dateMeasured: DateTime
    private lateinit var dateDialog: DatePickerDialog

    init {
        newDateMeasured = DateTime()
        dateMeasured = DateTime()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInsertEmployeeBinding.inflate(inflater, container, false)
        ToolbarUtils.initToolbar(
            activity as AppCompatActivity,
            binding.toolbar,
            getString(R.string.add_employee),
            displayHome = true,
            displayTitle = true
        )

        binding.toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        binding.etBirthday.setOnTouchListener(this)
        binding.etBirthday.showSoftInputOnFocus = false
        binding.etBirthday.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                activity?.let { Utility.hideKeyboard(it) }
                showDateDialog()
            }
        }
        val app = activity?.application as OutfitAppApplication
        employeeViewModel = ViewModelProvider(
            this,
            MyViewModelFactory(app)
        ).get(EmployeeViewModel::class.java)

        binding.btnAddEmployee.setOnClickListener(this)

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun showDateDialog() {
        if (!this::dateDialog.isInitialized) {
            initDialog()
        } else {
            dateDialog.updateDate(
                dateMeasured.year,
                dateMeasured.monthOfYear - 1,
                dateMeasured.dayOfMonth
            )
        }
        if (!dateDialog.isShowing) {
            dateDialog.show()
        }
    }

    private fun initDialog() {
        context?.let {
            dateDialog = DatePickerDialog(
                it,
                this,
                dateMeasured.year,
                dateMeasured.monthOfYear - 1,
                dateMeasured.dayOfMonth
            )
            dateDialog.datePicker.maxDate = System.currentTimeMillis()
        }
    }

    override fun onClick(v: View?) {
        if (v == binding.btnAddEmployee) {
            if (checkValidation()) {
                employeeViewModel.insertEmployee(getEmployee())
                Toast.makeText(activity, getString(R.string.save_success), Toast.LENGTH_SHORT)
                    .show()
                activity?.let {
                    Utility.hideKeyboard(it)
                    it.onBackPressed()
                }
            }
        }
    }

    private fun getEmployee(): Employee {
        val name: String = binding.etName.text.toString().trim()
        val birthday: String = binding.etBirthday.text.toString().trim()
        val salary: Double = binding.etSalary.text.toString().trim().toDouble()
        val gender: Gender = getGender()
        return Employee(
            id = 0,
            name = name,
            birthdayDate = birthday,
            salary = salary,
            gender = gender.value
        )
    }

    private fun getGender(): Gender {
        return if (binding.rgGender.checkedRadioButtonId == binding.rbMale.id) {
            Gender.MALE
        } else {
            Gender.FEMALE
        }
    }

    private fun checkValidation(): Boolean {
        val name: String = binding.etName.text.toString().trim()
        val birthday: String = binding.etBirthday.text.toString().trim()
        val salary: String = binding.etSalary.text.toString().trim()
        var isValid = true
        when {
            name.isEmpty() -> {
                binding.tilName.error = getString(R.string.error_name_mandatory)
                isValid = false
            }
        }
        when {
            birthday.isEmpty() -> {
                binding.tilBirthday.error = getString(R.string.error_birthday_mandatory)
                isValid = false
            }
        }
        when {
            salary.isEmpty() -> {
                binding.tilSalary.error = getString(R.string.error_salary_mandatory)
                isValid = false
            }
        }
        return isValid
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent?): Boolean {
        if (v == binding.etBirthday) {
            if (System.currentTimeMillis() - timestampClick < 500) return false
            timestampClick = System.currentTimeMillis()
            activity?.let { Utility.hideKeyboard(it) }
            showDateDialog()
        }
        return false
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        newDateMeasured = newDateMeasured
            .withYear(year)
            .withMonthOfYear(month + 1)
            .withDayOfMonth(dayOfMonth)
        validateDateTime()
    }

    private fun validateDateTime() {
        if (newDateMeasured.isBefore(DateTime())) {
            dateMeasured = newDateMeasured
        } else {
            dateMeasured = DateTime()
            newDateMeasured = DateTime()
        }
        binding.etBirthday.setText(JodaUtils.printShortDate(dateMeasured))
    }
}