package com.kg.test.outfit.service

/**
 * Created by KlemenG on 6. 04. 2020.
 */
interface OnServiceResultListener<T> {
    fun onError(t: Throwable?)
    fun onSuccess(`object`: T)
}