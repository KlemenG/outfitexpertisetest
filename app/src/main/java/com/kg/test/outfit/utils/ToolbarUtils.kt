package com.kg.test.outfit.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

class ToolbarUtils {
    companion object {
        fun initToolbar(
            activity: AppCompatActivity,
            toolbar: Toolbar,
            titleRef: String?,
            displayHome: Boolean,
            displayTitle: Boolean
        ) {
            activity.setSupportActionBar(toolbar)
            activity.title = titleRef
            val actionBar = activity.supportActionBar
            actionBar?.setDisplayShowTitleEnabled(displayTitle)
            actionBar?.setDisplayHomeAsUpEnabled(displayHome)
            actionBar?.setDisplayShowHomeEnabled(displayHome)
            actionBar?.setHomeButtonEnabled(displayHome)
        }
    }
}
