package com.kg.test.outfit.utils

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import org.joda.time.DateTime
import org.joda.time.Years
import timber.log.Timber
import kotlin.math.abs

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class Utility {
    companion object {
        const val DEFAULT_DATE_FORMAT: String = "dd/MM/yyyy"
        fun hideKeyboard(activity: Activity) {
            try {
                val inputManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            } catch (e: NullPointerException) {
                Timber.e("error hiding keyboard")
            }
        }

        fun getAge(date: DateTime?): Int {
            if (date == null) return 0
            val ageInYears = Years.yearsBetween(date, DateTime())
            return abs(ageInYears.years)
        }
    }
}