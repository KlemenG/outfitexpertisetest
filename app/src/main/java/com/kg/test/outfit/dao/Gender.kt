package com.kg.test.outfit.dao

import com.kg.test.outfit.R

/**
 * Created by KlemenG on 5. 04. 2020.
 */
enum class Gender(genderInt: Int, val resourceId: Int) {
    MALE(0, R.string.male),
    FEMALE(1, R.string.female);

    val value: Int = genderInt
}