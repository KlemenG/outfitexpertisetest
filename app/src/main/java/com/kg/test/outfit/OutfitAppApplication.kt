package com.kg.test.outfit

import android.app.Application
import timber.log.Timber

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class OutfitAppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}