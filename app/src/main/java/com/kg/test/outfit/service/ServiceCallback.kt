package com.kg.test.outfit.service

import com.kg.test.outfit.utils.ErrorUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by KlemenG on 6. 04. 2020.
 */
class ServiceCallback<T>(
    private val onServiceResultListener: OnServiceResultListener<T>
) : Callback<T?> {
    override fun onResponse(call: Call<T?>, response: Response<T?>) {
        if (response.isSuccessful) {
            response.body()?.let { onServiceResultListener.onSuccess(it) }
        } else {
            val error: ErrorResult? =
                ErrorUtils.parseError(response)
            onServiceResultListener.onError(error?.getError())
        }
    }

    override fun onFailure(call: Call<T?>, t: Throwable) {
        onServiceResultListener.onError(t)
    }
}