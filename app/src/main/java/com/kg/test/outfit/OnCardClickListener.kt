package com.kg.test.outfit

import android.view.View

/**
 * Created by KlemenG on 4. 04. 2020.
 */
interface OnCardClickListener {
    fun onCardClick(view: View?)
}