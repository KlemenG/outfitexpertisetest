package com.kg.test.outfit.fragments

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.kg.test.outfit.OutfitAppApplication
import com.kg.test.outfit.R
import com.kg.test.outfit.data.EmployeeViewModel
import com.kg.test.outfit.databinding.FragmentEmployeeDetailsBinding
import com.kg.test.outfit.service.objects.SearchResult
import com.kg.test.outfit.utils.MyViewModelFactory
import com.kg.test.outfit.utils.ToolbarUtils
import com.kg.test.outfit.vao.Employee
import timber.log.Timber

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class EmployeeDetailsFragment : Fragment() {
    private var _binding: FragmentEmployeeDetailsBinding? = null
    private val binding get() = _binding!!
    private lateinit var employeeViewModel: EmployeeViewModel
    private lateinit var _employee: Employee
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmployeeDetailsBinding.inflate(inflater, container, false)

        ToolbarUtils.initToolbar(
            activity as AppCompatActivity,
            binding.toolbar,
            getString(R.string.title_employee),
            displayHome = true,
            displayTitle = true
        )
        binding.toolbar.setNavigationOnClickListener { activity?.onBackPressed() }

        binding.dvName.setTitle(getString(R.string.display_name))
        binding.dvAge.setTitle(getString(R.string.display_age))
        binding.dvGender.setTitle(getString(R.string.display_gender))
        binding.dvSalary.setTitle(getString(R.string.display_salary))

        val app = activity?.application as OutfitAppApplication
        employeeViewModel = ViewModelProvider(
            this,
            MyViewModelFactory(app)
        ).get(EmployeeViewModel::class.java)
        val employeeId = arguments?.getLong(EmployeeListFragment.EXTRA_EMPLOYEE_ID)
        employeeId?.let {
            employeeViewModel.getEmployeeById(it)
                .observe(
                    viewLifecycleOwner,
                    Observer { employee ->
                        employee?.let {
                            _employee = employee
                            binding.dvName.setValue(employee.name)
                            binding.dvAge.setValue(employee.getAge().toString())
                            binding.dvGender.setValue(getString(employee.gender().resourceId))
                            binding.dvSalary.setValue(employee.salary.toString())
                            getData(employee.name)
                        }
                    })
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_delete, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        view?.let {
            if (item.itemId == R.id.delete) {
                employeeViewModel.deleteItem(_employee)
                Toast.makeText(context, getString(R.string.employee_deleted), Toast.LENGTH_LONG)
                    .show()
                activity?.onBackPressed()
            }
        }
        return false
    }

    private fun getData(search: String) {
        binding.progressbar.visibility = View.VISIBLE
        employeeViewModel.getEmployeeSearch(context, search)
            .observe(viewLifecycleOwner, Observer { response ->
                binding.progressbar.visibility = View.GONE
                if (response.isError) {
                    response.throwable?.let {
                        Timber.e(response.throwable)
                    }
                } else {
                    response.result?.let { handleResult(it) }
                }
            })
    }

    private fun handleResult(`object`: SearchResult) {
        `object`.searchItems?.let {
            var searches = ""
            var topFive = it
            var i = 0
            if (topFive.isNotEmpty()) {
                if (topFive.size >= 5) {
                    topFive = it.take(5)
                }
                topFive.forEach { searchItem ->
                    searches += if (i == 0) {
                        searchItem.title
                    } else {
                        "\n\n${searchItem.title}"
                    }
                    i++
                }
            }
            binding.tvSearches.text = searches
        }
    }
}