package com.kg.test.outfit.vao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.kg.test.outfit.dao.Gender
import com.kg.test.outfit.utils.Utility
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * Created by KlemenG on 4. 04. 2020.
 */
@Entity(tableName = "employee")
class Employee(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Long,
    @ColumnInfo(name = "Name")
    val name: String,
    @ColumnInfo(name = "Birthday_date")
    val birthdayDate: String,
    @ColumnInfo(name = "Gender")
    val gender: Int,
    @ColumnInfo(name = "Salary")
    val salary: Double
) {
    private fun birthdayDate() : DateTime {
        return DateTime.parse(birthdayDate, DateTimeFormat.forPattern(Utility.DEFAULT_DATE_FORMAT))
    }

    fun gender() : Gender {
        return Gender.values()[gender]
    }

    fun getAge(): Int {
        return Utility.getAge(birthdayDate())
    }
}