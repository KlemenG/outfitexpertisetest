package com.kg.test.outfit.utils

import com.kg.test.outfit.service.ServiceGenerator
import com.kg.test.outfit.service.ErrorResult
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response

/**
 * Created by KlemenG on 6. 04. 2020.
 */
object ErrorUtils {
    fun parseError(response: Response<*>): ErrorResult? {
        val converter: Converter<ResponseBody?, ErrorResult> = ServiceGenerator.retrofit
            .responseBodyConverter(ErrorResult::class.java, arrayOfNulls<Annotation>(0))
        val error: ErrorResult?
        error = try {
            response.errorBody()?.let { converter.convert(it) }
        } catch (e: Exception) {
            val errorResult = ErrorResult()
            errorResult.setError(Error(e.message))
            return errorResult
        }
        return error
    }
}