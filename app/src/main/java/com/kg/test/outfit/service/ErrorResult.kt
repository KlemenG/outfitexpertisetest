package com.kg.test.outfit.service

/**
 * Created by KlemenG on 6. 04. 2020.
 */
class ErrorResult {
    private var error: Error? = null
    fun getError(): Error? {
        return error
    }

    fun setError(error: Error?) {
        this.error = error
    }
}