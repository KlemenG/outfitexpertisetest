package com.kg.test.outfit.service

import com.kg.test.outfit.service.objects.SearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by KlemenG on 5. 04. 2020.
 */
interface ServiceInterface {
    @GET("customsearch/v1")
    fun getSearch(
        @Query("key") key: String,
        @Query("cx") cx: String,
        @Query("q") query: String
    ): Call<SearchResult>
}