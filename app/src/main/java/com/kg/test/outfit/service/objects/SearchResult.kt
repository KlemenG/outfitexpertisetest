package com.kg.test.outfit.service.objects

import com.google.gson.annotations.SerializedName
import com.kg.test.outfit.vao.SearchItem

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class SearchResult(
    @SerializedName("items")
    var searchItems: List<SearchItem>?
)