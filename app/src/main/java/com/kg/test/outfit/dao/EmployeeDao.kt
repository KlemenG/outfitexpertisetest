package com.kg.test.outfit.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.kg.test.outfit.vao.Employee

/**
 * Created by KlemenG on 4. 04. 2020.
 */
@Dao
interface EmployeeDao {
    @Query("SELECT * from employee ORDER BY name ASC")
    fun getEmployees(): LiveData<List<Employee>>

    @Query("SELECT * from employee where id = :employeeId")
    fun getEmployeeById(employeeId: Long): LiveData<Employee>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmployee(employee: Employee)

    @Delete
    suspend fun deleteEmployee(employee: Employee)
}