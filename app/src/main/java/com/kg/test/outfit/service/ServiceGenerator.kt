package com.kg.test.outfit.service

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class ServiceGenerator {
    companion object {
        private const val BASE_URL: String = "https://www.googleapis.com/"
        const val CX: String = "005850993763660260790:qssfenop8gl"
        private const val TIMEOUT: Long = 20
        lateinit var retrofit: Retrofit
        val getClient: ServiceInterface
            get() {
                val gson = GsonBuilder()
                    .setLenient()
                    .create()
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client =
                    provideOkHttpClient()

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

                return retrofit.create(ServiceInterface::class.java)
            }

        private fun provideOkHttpClient(): OkHttpClient {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor { chain: Interceptor.Chain ->
                    val request = chain.request().newBuilder()
                        .addHeader(
                            "Content-Type",
                            "application/json"
                        )
                        .build()
                    chain.proceed(request)
                }
                .build()
        }
    }
}
