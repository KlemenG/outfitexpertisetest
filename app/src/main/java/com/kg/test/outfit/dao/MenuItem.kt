package com.kg.test.outfit.dao

import com.kg.test.outfit.R

/**
 * Created by KlemenG on 4. 04. 2020.
 */
enum class MenuItem(stringResource: Int) {
    ITEM_EMPLOYEE_LIST(R.string.title_employee_list),
    ITEM_EMPLOYEE_STATISTICS(R.string.title_employee_analytics);

    val titleRes: Int = stringResource
}