package com.kg.test.outfit.utils

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class JodaUtils {
    companion object {
        fun printShortDate(dateTime: DateTime?): String? {
            return printDate(dateTime, DateTimeFormat.shortDate())
        }

        private fun printDate(
            dateTime: DateTime?,
            formatter: DateTimeFormatter
        ): String? {
            return formatter.print(dateTime)
        }
    }
}
