package com.kg.test.outfit.data

import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.kg.test.outfit.OutfitAppApplication
import com.kg.test.outfit.R
import com.kg.test.outfit.service.OnServiceResultListener
import com.kg.test.outfit.service.ServiceCallback
import com.kg.test.outfit.service.ServiceGenerator
import com.kg.test.outfit.vao.Employee
import com.kg.test.outfit.service.objects.ResponseObject
import com.kg.test.outfit.service.objects.SearchResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call

/**
 * Created by KlemenG on 4. 04. 2020.
 */
class EmployeeViewModel(application: OutfitAppApplication) : AndroidViewModel(application),
    OnServiceResultListener<SearchResult> {
    private val repository: EmployeeRepository
    val allEmployees: LiveData<List<Employee>>
    private var searchResult: MediatorLiveData<ResponseObject> = MediatorLiveData<ResponseObject>()

    init {
        val employeeDao = EmployeeDatabase.getDatabase(application).employeeDao()
        repository = EmployeeRepository(employeeDao)
        allEmployees = repository.allEmployees
    }

    fun getEmployeeById(employeeId: Long): LiveData<Employee> {
        return repository.getEmployeeById(employeeId)
    }

    fun insertEmployee(employee: Employee) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertEmployee(employee)
    }

    fun deleteItem(employee: Employee) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteEmployee(employee)
    }

    fun getEmployeeSearch(context: Context?, search: String) : LiveData<ResponseObject> {
        callSearchEmployee(context, search)
        return searchResult
    }

    private fun callSearchEmployee(context: Context?, search: String) {
        context?.let {
            val call: Call<SearchResult> = ServiceGenerator.getClient.getSearch(
                context.getString(R.string.google_key),
                ServiceGenerator.CX,
                search
            )
            call.enqueue(ServiceCallback(this))
        }
    }

    override fun onError(t: Throwable?) {
        searchResult.postValue(
            ResponseObject(
                true,
                t,
                null
            )
        )
    }

    override fun onSuccess(`object`: SearchResult) {
        searchResult.postValue(
            ResponseObject(
                false,
                null,
                `object`
            )
        )
    }
}