package com.kg.test.outfit.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kg.test.outfit.OnCardClickListener
import com.kg.test.outfit.R
import com.kg.test.outfit.vao.Employee

class EmployeesAdapter(
    private val context: Context?,
    private val callback: OnCardClickListener
) : RecyclerView.Adapter<EmployeesAdapter.ViewHolder>(), View.OnClickListener {
    private var employees = emptyList<Employee>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_menu_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val employee = employees[position]
        val vh: ViewHolder = holder

        vh.title.text = employee.name
        vh.cardView.tag = employee.id
        vh.cardView.setOnClickListener(this)
    }

    override fun getItemCount(): Int {
        return employees.size
    }

    fun setEmployees(employees: List<Employee>) {
        this.employees = employees
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tvTitle)
        val cardView: CardView = itemView.findViewById(R.id.cardView)
    }

    override fun onClick(v: View?) {
        callback.onCardClick(v)
    }

}
