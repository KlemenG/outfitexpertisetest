package com.kg.test.outfit.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kg.test.outfit.OutfitAppApplication
import com.kg.test.outfit.data.EmployeeViewModel

/**
 * Created by KlemenG on 22. 04. 2020.
 */
class MyViewModelFactory(
    private val app: OutfitAppApplication
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return EmployeeViewModel(app) as T
    }
}