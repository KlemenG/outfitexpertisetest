package com.kg.test.outfit.view

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.kg.test.outfit.databinding.ViewDataBinding

fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density.toInt())

/**
 * Created by KlemenG on 5. 04. 2020.
 */
class DataView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private var binding: ViewDataBinding =
        ViewDataBinding.inflate(LayoutInflater.from(context), this)

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val margin5 = 5.toPx()
        val margin30 = 30.toPx()
        val customParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        customParams.marginStart = margin30
        customParams.marginEnd = margin30
        customParams.topMargin = margin5
        customParams.bottomMargin = margin5
        binding.root.layoutParams = customParams
    }

    fun setTitle(title: String) {
        binding.tvTitle.text = title
    }

    fun setValue(value: String) {
        binding.tvValue.text = value
    }
}